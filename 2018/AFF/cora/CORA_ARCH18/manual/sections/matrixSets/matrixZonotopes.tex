A matrix zonotope is defined analogously to zonotopes (see Sec. \ref{sec:zonotope}):
\begin{equation}\label{eq:matrixZonotope}
	\mathcal{A}_{[z]}=\Big\{ G\^{0}+\sum_{i=1}^{\kappa} p_i G\^{i} \Big| p_i \in [-1,1], G\^{i}\in\mathbb{R}^{n \times n} \Big\}
\end{equation}
and is written in short form as $\mathcal{A}_{[z]}=(G\^0, G\^1, \ldots , G\^\kappa)$, where the first matrix is referred to as the \textit{matrix center} and the other matrices as \textit{matrix generators}. The order of a matrix zonotope is defined as $\rho = \kappa/n$. When exchanging the matrix generators by vector generators $g\^{i}\in \mathbb{R}^{n}$, one obtains a zonotope (see e.g. \cite{Girard2005}). 

We support the following methods for zonotope matrices:
\begin{itemize}
 \item \texttt{concatenate} -- concatenates the center and all generators of two matrix zonotopes.
 \item \texttt{display} -- standard method, see Sec. \ref{sec:matrixSetRepresentationsAndOperations}.
 \item \texttt{dependentTerms} -- considers dependency in the computation of Taylor terms for the matrix zonotope exponential according to \cite[Proposition 4.3]{Althoff2011b}. 
 \item \texttt{dominantVertices} -- computes the dominant vertices of a matrix zonotope according to a heuristics.
 \item \texttt{expmInd} -- operator for the exponential matrix of a matrix zonotope, evaluated independently.
 \item \texttt{expmIndMixed} -- operator for the exponential matrix of a matrix zonotope, evaluated independently. Higher order terms are computed
via interval arithmetic.
 \item \texttt{expmMixed} -- operator for the exponential matrix of a matrix zonotope, evaluated dependently. Higher order terms are computed
via interval arithmetic as discussed in \cite[Section 4.4.4]{Althoff2011b}.
 \item \texttt{expmOneParam} -- operator for the exponential matrix of a matrix zonotope when only one parameter is uncertain as described in \cite[Theorem 1]{Althoff2012b}.
 \item \texttt{expmVertex} -- computes the exponential matrix for a selected number of dominant vertices obtained by the \texttt{dominantVertices} method.
 \item \texttt{infNorm} -- returns the maximum of the infinity norm of a matrix zonotope.
 \item \texttt{infNormRed} -- returns a faster over-approximation of the maximum of the infinity norm of a matrix zonotope by reducing its representation size in an over-approximative way.
 \item \texttt{intervalMatrix} -- standard method, see Sec. \ref{sec:matrixSetRepresentationsAndOperations}.
 \item \texttt{matPolytope} -- converts a matrix zonotope into a matrix polytope representation.
 \item \texttt{matZonotope} -- constructor of the class.
 \item \texttt{mpower} -- overloaded '${}^{\wedge}$' operator for the power of matrix zonotopes.
 \item \texttt{mtimes} -- standard method, see Sec. \ref{sec:matrixSetRepresentationsAndOperations} for numeric matrix multiplication or a multiplication with another matrix zonotope according to \cite[Equation 4.10]{Althoff2011b}.
 \item \texttt{plot} -- plots 2-dimensional projection of a matrix zonotope.
 \item \texttt{plus} -- standard method (see Sec. \ref{sec:matrixSetRepresentationsAndOperations}) for a matrix zonotope or a numerical matrix. 
 \item \texttt{powers} -- computes the powers of a matrix zonotope up to a certain order.
 \item \texttt{reduce} -- reduces the order of a matrix zonotope. This is done by converting the matrix zonotope to a zonotope, reducing the zonotope, and converting the result back to a matrix zonotope.
 \item \texttt{uniformSampling} -- creates samples uniformly within a matrix zonotope.
 \item \texttt{vertices} -- standard method, see Sec. \ref{sec:matrixSetRepresentationsAndOperations}.
 \item \texttt{volume} -- computes the volume of a matrix zonotope by computing the volume
of the corresponding zonotope.
 \item \texttt{zonotope} -- converts a matrix zonotope into a zonotope.
\end{itemize}


Since the matrix zonotope class is written using the new structure for object oriented programming in MATLAB, it has the following public properties:
\begin{itemize}
 \item \texttt{dim} -- dimension.
 \item \texttt{gens} -- number of generators.
 \item \texttt{center} -- $G\^0$ according to \eqref{eq:matrixZonotope}.
 \item \texttt{generator} -- cell array of matrices $G\^{i}$ according to \eqref{eq:matrixZonotope}.
\end{itemize}


\subsubsection{Matrix Zonotope Example} \label{sec:matrixZonotopeExample}

The following MATLAB code demonstrates some of the introduced methods:

{\small
\input{./MATLABcode/matZonotope_test.tex}}

This produces the workspace output
\begin{verbatim}
dimension: 
     2

nr of generators: 
     2

center: 
     0     4
     5     3

generators: 
     1     0
     1     1

---------------
         0    0.5000
    0.5000         0

---------------
dimension: 
     1

nr of generators: 
     3

center: 
     3     0
     5     2

generators: 
    1.0000    0.5000
    2.0000    1.5000

---------------
    -1     2
     1     1

---------------
         0    0.5000
    0.5000    0.5000

---------------
dimension: 
     2

left limit: 
     0     2
     2     3

right limit: 
     2     2
     4     5
\end{verbatim}