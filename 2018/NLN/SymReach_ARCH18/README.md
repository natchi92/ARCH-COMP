Requirements:
--------------------------------

    Gnuplot: http://gnuplot.info/
    Eigen: http://eigen.tuxfamily.org/index.php?title=Main_Page
    Ginac: https://ginac.de/Download.html

Please edit SymReach_ARCH18/examples/makefile to adjust directory paths.

Libraries to be linked: -lboost_filesystem -lboost_iostreams -lboost_system -lginac -lcln

If compiler else than g++ then please edit SymReach_ARCH18/src/ReachableSet2cpp.h: line 1689

       system("g++ -shared func_from_file.cpp -o func_from_file.so");  
to adjust compiler.



Repeatability:
-----------------------------------
cd examples  
make

This will create executables for the examples in their respective folders; and they are executed sequentially; generates plots and gives reachable set computation times.
